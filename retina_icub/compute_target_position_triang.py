from __future__ import division
# Imported Python Transfer Function
#
# Compute Target position following a triangular trajectory
@nrp.MapVariable("target_freq",scope=nrp.GLOBAL)
@nrp.MapVariable("target_ampl", scope=nrp.GLOBAL)
@nrp.MapVariable("target_delta", scope=nrp.GLOBAL)
@nrp.MapVariable("trajectory", scope=nrp.GLOBAL)
@nrp.Robot2Neuron()
def compute_target_position_triang(t, target_freq, target_ampl, target_delta, trajectory):
    from past.utils import old_div

    if trajectory != "triangular":
        return
    frequency = target_freq.value
    amplitude = target_ampl.value
    T = old_div(1.,frequency)
    if t % T <= old_div(T,4.):
        target_delta.value = (old_div((t % (old_div(T, 4.))), (old_div(T, 4.)))) * (old_div(float(amplitude), 2.))
    elif t % T > old_div(T,4.) and t % T <= 3*T/4.:
        target_delta.value = (old_div(float(amplitude), 2.)) - (old_div(((t - old_div(T,4.)) % (old_div(T, 2.))), (old_div(T, 2.)))) * (float(amplitude))
    else:
        target_delta.value = (old_div((t % (old_div(T, 4.))), (old_div(T, 4.)))) * (old_div(float(amplitude), 2.)) - (old_div(float(amplitude), 2.))
#

