<?xml version="1.0" ?>
<!-- Note: Typical bone density: 1900 kg/m3 -->
<!--Density of Al: 2700 kg/m3-->
<!--Reference for strength of some mouse hindlimb muscle:
    https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4549925/
    Maximal force generation was about 40g, i.e. ca. 0.4N.
    -->

<sdf version="1.6">
  <model name="knee_jerk">
    <link name="thigh">
    <pose frame=''>0 0 1 0 -0 0</pose>
    <inertial>
      <pose frame=''>0 0 0 0 -0 0</pose>
      <mass>5</mass>
	<inertia>
	  <ixx>1</ixx>
	  <ixy>0</ixy>
	  <ixz>0</ixz>
	  <iyy>1</iyy>
	  <iyz>0</iyz>
	  <izz>1</izz>
	</inertia>
    </inertial>
    <collision name='thigh_collision'>
	<pose frame=''>0 0.25 0 1.5708 -0 0</pose>
	<geometry>
	  <cylinder>
	    <length>0.5</length>
	    <radius>0.05</radius>
	  </cylinder>
	</geometry>
    </collision>
    <visual name='thigh_visual'>
	<pose frame=''>0 0.25 0 1.5708 -0 0</pose>
	<geometry>
	  <mesh>
            <scale>1 1 1</scale>
            <uri>model://opensim_knee_jerk_tut/meshes/thigh.dae</uri>
          </mesh>
	</geometry>
	<material>
          <script>
            <name>Gazebo/Grey</name>
            <uri>file://media/materials/scripts/gazebo.material</uri>
          </script>
        </material>
    </visual>
    </link>

    <link name="shank">
    <pose frame=''>0 0.5 1 0 -0 0</pose>
    <inertial>
      <pose frame=''>0 0 0 0 -0 0</pose>
      <mass>5</mass>
      <inertia>
	<ixx>1</ixx>
	  <ixy>0</ixy>
	  <ixz>0</ixz>
	  <iyy>1</iyy>
	  <iyz>0</iyz>
	  <izz>1</izz>
      </inertia>
    </inertial>
    <collision name='shank_collision'>
      <pose frame=''>0 0 -0.25 0 -0 0</pose>
	<geometry>
	  <cylinder>
	    <length>0.5</length>
	    <radius>0.01</radius>
	  </cylinder>
	</geometry>
    </collision>
    <visual name='shank_visual'>
      <pose frame=''>0 0.05 -0.3 0 -0 0</pose>
	<geometry>
	  <mesh>
            <scale>1 1 1</scale>
            <uri>model://opensim_knee_jerk_tut/meshes/shank.dae</uri>
          </mesh>
	</geometry>
	<material>
          <script>
            <name>Gazebo/Grey</name>
            <uri>file://media/materials/scripts/gazebo.material</uri>
          </script>
        </material>
    </visual>
    </link>

    <joint name='hip' type='fixed'>
      <child>thigh</child>
      <parent>world</parent>
    </joint>

    <joint name='knee' type='revolute'>
      <child>shank</child>
      <parent>thigh</parent>
      <axis>
        <xyz>1 0 0</xyz>
        <dynamics>
          <damping>1.e-6</damping>
        </dynamics>
        <limit>
            <lower>-1.046</lower>
            <upper>1.046</upper>
            <stiffness>100</stiffness>
            <dissipation>1.e-2</dissipation>
        </limit>
      </axis>
    </joint>

    <muscles>model://opensim_knee_jerk_tut/muscles.osim</muscles>

    <plugin name="muscle_interface_plugin" filename="libgazebo_ros_muscle_interface.so"></plugin>
  </model>
</sdf>
