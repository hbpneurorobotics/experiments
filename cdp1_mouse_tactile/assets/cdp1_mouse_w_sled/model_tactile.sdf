<?xml version="1.0" ?>
<!-- Note: Typical bone density: 1900 kg/m3 -->
<!--Density of Al: 2700 kg/m3-->
<!--Reference for strength of some mouse hindlimb muscle:
    https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4549925/
    Maximal force generation was about 40g, i.e. ca. 0.4N.
    -->

<sdf version="1.6">
  <model name='mouse_and_sled'>
    <self_collide>true</self_collide>
    <link name="humerus">
      <pose>1.021798e-02 -6.925863e-02 2.393958e-02 1.463 0.736 1.094</pose>
<inertial>
  <pose>4.13950e-03 -1.77810e-04 -1.08265e-04 0 0 0</pose>
  <mass>5.21922e-05</mass>
  <inertia>
    <ixx>3.08473e-11</ixx>
    <iyy>7.57217e-10</iyy>
    <izz>7.65936e-10</izz>
    <ixy>-2.65578e-11</ixy>
    <ixz>1.69743e-11</ixz>
    <iyz>2.61988e-12</iyz>
  </inertia>
</inertial>
      <visual name="visual">
        <geometry>
          <mesh>
            <scale>0.0015 0.0015  0.0015</scale>
            <uri>model://cdp1_mouse_w_sled/meshes/UpperLeg.dae</uri>
          </mesh>
        </geometry>
        <material>
          <script>
            <name>Gazebo/Grey</name>
            <uri>file://media/materials/scripts/gazebo.material</uri>
          </script>
        </material>
      </visual>
    </link>

    <link name="radius">
      <pose>1.380963e-02 -6.230082e-02 1.685169e-02 -0.107 0.186 -1.566</pose>
<inertial>
  <pose>3.60719e-03 -3.44408e-06 -5.31513e-04 0 0 0</pose>
  <mass>2.71493e-05</mass>
  <inertia>
    <ixx>2.21656e-11</ixx>
    <iyy>5.68533e-10</iyy>
    <izz>5.53422e-10</izz>
    <ixy>-1.29702e-11</ixy>
    <ixz>-8.09620e-11</ixz>
    <iyz>-1.75862e-12</iyz>
  </inertia>
</inertial>
      <visual name="visual">
        <geometry>
          <mesh>
            <scale>0.0015 0.0015  0.0015</scale>
            <uri>model://cdp1_mouse_w_sled/meshes/LowerLeg.dae</uri>
          </mesh>
        </geometry>
        <material>
          <script>
            <name>Gazebo/Grey</name>
            <uri>file://media/materials/scripts/gazebo.material</uri>
          </script>
        </material>
      </visual>
      <collision name="collider">
        <pose>1.274393e-02 6.153062e-05 -1.766485e-04 1.714 -0.009 -0.023</pose>
        <geometry>
          <mesh>
            <scale>0.0017 0.0017  0.0017</scale>
            <uri>model://cdp1_mouse_w_sled/meshes/arm_collider.dae</uri>
          </mesh>
        </geometry>
        <surface>
          <contact>
            <opensim>
              <stiffness>7.e8</stiffness>
              <dissipation>1.</dissipation>
            </opensim>
          </contact>
        </surface>
      </collision>
      <!-- <visual name="collidervis">
        <pose>1.274393e-02 6.153062e-05 -1.766485e-04 1.714 -0.009 -0.023</pose>
        <geometry>
          <mesh>
            <scale>0.0017 0.0017  0.0017</scale>
            <uri>model://cdp1_mouse_w_sled/meshes/arm_collider.dae</uri>
          </mesh>
        </geometry>
      </visual> -->
    </link>

    <link name="foot">
    <pose>1.388944e-02 -7.482718e-02 1.472293e-02 1.516 0.065 -1.543</pose>
<inertial>
  <pose>3.13744e-03 -4.07710e-04 2.34798e-05 0 0 0</pose>
  <mass>9.10633e-06</mass>
  <inertia>
    <ixx>3.16374e-12</ixx>
    <iyy>3.98443e-11</iyy>
    <izz>3.72875e-11</izz>
    <ixy>-1.54496e-12</ixy>
    <ixz>-1.75061e-13</ixz>
    <iyz>-1.33752e-14</iyz>
  </inertia>
</inertial>

      <visual name="visual">
        <geometry>
          <mesh>
            <scale>0.0015 0.0015  0.0015</scale>
            <uri>model://cdp1_mouse_w_sled/meshes/Foot_simplified.dae</uri>
          </mesh>
        </geometry>
        <material>
          <script>
            <name>Gazebo/Grey</name>
            <uri>file://media/materials/scripts/gazebo.material</uri>
          </script>
        </material>
      </visual>

      <collision name="collider">
        <pose>-3.114162e-04 -5.070098e-04 4.908681e-05 0.057 0.012 0.025</pose>
        <geometry>
          <mesh>
            <scale>0.0017 0.0017  0.0017</scale>
            <uri>model://cdp1_mouse_w_sled/meshes/hand_collider.dae</uri>
          </mesh>
        </geometry>
        <surface>
          <contact>
            <opensim>
              <stiffness>7.e8</stiffness>
              <dissipation>1.</dissipation>
            </opensim>
          </contact>
        </surface>
      </collision>
      <!-- <visual name="collidervis">
        <pose>-3.114162e-04 -5.070098e-04 4.908681e-05 0.057 0.012 0.025</pose>
        <geometry>
          <mesh>
            <scale>0.0017 0.0017  0.0017</scale>
            <uri>model://cdp1_mouse_w_sled/meshes/hand_collider.dae</uri>
          </mesh>
        </geometry>
      </visual> -->
    </link>

    <joint name="world_humerus" type="ball">
      <pose>3.814697e-06 1.907349e-06 0.000000e+00 -1.343 -1.274 0.089</pose>
      <parent>world</parent>
      <child>humerus</child>
<!--      <axis>
        <xyz>1 0 0</xyz>
        <dynamics>
          <damping>1.e-7</damping>
        </dynamics>
        <limit>
            <lower>-2.44</lower>
            <upper>0.</upper>
            <stiffness>1</stiffness>
            <dissipation>1.e-3</dissipation>
        </limit>
      </axis>-->
    </joint>

    <joint name="humerus_radius" type="revolute">
      <pose>0.000000e+00 -9.536743e-07 -1.907349e-06 -1.142 -0.020 1.611</pose>
      <parent>humerus</parent>
      <child>radius</child>
      <axis>
        <xyz>1 0 0</xyz>
        <dynamics>
          <damping>1.e-6</damping>
        </dynamics>
        <limit>
            <lower>-1.046</lower>
            <upper>1.046</upper>
            <stiffness>100</stiffness>
            <dissipation>1.e-2</dissipation>
        </limit>
      </axis>
    </joint>

    <joint name="radius_foot" type="revolute">
      <pose>1.144409e-05 9.536743e-07 -4.768372e-07 0.569 1.483 2.192</pose>
      <parent>radius</parent>
      <child>foot</child>
      <axis>
        <xyz>1 0 0</xyz>
        <dynamics>
          <damping>1.e-6</damping>
        </dynamics>
        <limit>
            <lower>-0.8722</lower>
            <upper>0.6</upper>
            <stiffness>100</stiffness>
            <dissipation>1.e-2</dissipation>
        </limit>
      </axis>
    </joint>

    <!-- <joint name="slider_foot" type="prismatic">
      <parent>foot</parent>
      <child>cdp1_msled::sled</child>
      <pose>0 0 1.e-3 0 0 0</pose>
      <axis>
      <xyz>1 0 0</xyz>
        <limit>
          <lower>-0.01</lower>
          <upper>0.01</upper>
          <stiffness>1.</stiffness>
          <dissipation>1.</dissipation>
        </limit>
      </axis>
    </joint> -->

    <include>
      <uri>model://cdp1_msled</uri>
    </include>

    <muscles>model://cdp1_mouse_w_sled/muscles.osim</muscles>

    <!-- <plugin name="generic_controller" filename="libgeneric_controller_plugin.so">
    </plugin> -->

    <plugin name="muscle_interface_plugin" filename="libgazebo_ros_muscle_interface.so"></plugin>
  </model>
</sdf>
