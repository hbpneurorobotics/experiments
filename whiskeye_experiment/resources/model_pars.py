from yaca_platform import *

################################################################
##	GEOMETRIC PARAMETERS

# values from CAD_review/Frames
WHISKY_LOC_LIFT_X = 0.12865
WHISKY_LOC_LIFT_Y = 0.0
WHISKY_LOC_LIFT_Z = 0.07985

# values arbitrary
WHISKY_LIFT_INI_RAD = np.radians(-45.0)
WHISKY_LIFT_MIN_RAD = np.radians(-60.0)
WHISKY_LIFT_MAX_RAD = np.radians(-30.0)

# values from CAD_review/Frames
WHISKY_LOC_PITCH_X = 0.24733
WHISKY_LOC_PITCH_Y = 0.0
WHISKY_LOC_PITCH_Z = -0.03100

# values arbitrary
WHISKY_PITCH_INI_RAD = np.radians(45.0)
WHISKY_PITCH_MIN_RAD = np.radians(15.0)
WHISKY_PITCH_MAX_RAD = np.radians(75.0)

# values from CAD_review/Frames
WHISKY_LOC_YAW_X = 0.10381
WHISKY_LOC_YAW_Y = 0.0
WHISKY_LOC_YAW_Z = 0.02250

# values arbitrary
WHISKY_YAW_INI_RAD = np.radians(0.0)
WHISKY_YAW_MIN_RAD = np.radians(-45.0)
WHISKY_YAW_MAX_RAD = np.radians(45.0)


################################################################
##	DEFINITION OF HEADSPACE

def find_nearest(array, value):
    return (np.abs(array - value)).argmin()


def get_range(i, L, N):
    return list(range(max(0, i - L), min(N, i + L + 1)))


class HeadSpace:

    def lim2range(self, lim, step):

        if -lim[0] == lim[1]:
            xx = np.arange(0, lim[1] + 1e-9, step)
            return np.concatenate((-xx[:0:-1], xx))
        else:
            return np.arange(lim[0], lim[1] + 1e-9, step)

    def __init__(self, xlim, ylim, zlim, step):

        self.xlim = xlim
        self.ylim = ylim
        self.zlim = zlim
        self.step = step

        self.xx = self.lim2range(xlim, step)
        self.yy = self.lim2range(ylim, step)
        self.zz = self.lim2range(zlim, step)

        self.shape = [len(self.xx), len(self.yy), len(self.zz)]
        self.numel = np.prod(self.shape)
        self.xi_cen = int(float((self.shape[0] - 1)) / 2 + 0.5)
        self.yi_cen = int(float((self.shape[1] - 1)) / 2 + 0.5)
        self.zi_cen = int(float((self.shape[2] - 1)) / 2 + 0.5)

        self.xii = list(range(0, self.shape[0]))
        self.yii = list(range(0, self.shape[1]))
        self.zii = list(range(0, self.shape[2]))

    def createMap(self):
        mapval = np.zeros(self.shape, dtype='float32')
        shape = self.shape[:]
        shape.append(3)
        mappos = np.zeros(shape)
        for xi in range(0, self.shape[0]):
            for yi in range(0, self.shape[1]):
                for zi in range(0, self.shape[2]):
                    mappos[xi, yi, zi] = np.array([self.xx[xi], self.yy[yi], self.zz[zi]])
        return (mappos, mapval)

    def getRanges(self, pos, L):

        # find best match
        xi = find_nearest(self.xx, pos[0])
        yi = find_nearest(self.yy, pos[1])
        zi = find_nearest(self.zz, pos[2])

        # generate range
        xii = get_range(xi, L, self.shape[0])
        yii = get_range(yi, L, self.shape[1])
        zii = get_range(zi, L, self.shape[2])

        return (xii, yii, zii)


################################################################
##	SYSTEM PARAMETERS OBJECT

class SystemPars(PlatformPars):

    def __init__(self):
        # super
        PlatformPars.__init__(self)

        # debug
        self.debug_actions = True

        # ROS
        self.topic_root = '/model'

        # reafferent noise model parameters
        # self.reaff = get_reaff()

        # contact belief threshold should be set to just clear of
        # the free-whisking noise floor for each column
        # contact_thresh = [0.02, 0.025, 0.03, 0.04]
        contact_thresh = [0.3] * 4
        self.contact_thresh = np.transpose(
            np.tile(np.transpose([contact_thresh]), self.shape_lo[0]))

        # contact belief gain is a per-column parameter, for now we
        # just set it to the same across all columns - a sensible
        # value would be between 1 and 10, say.
        self.contact_gain = 5.0  # * np.ones(self.shape_lo)

        # spatial pars
        self.headspace = HeadSpace([0.0, 0.6], [-0.5, 0.5], [-0.3, 0.3], 0.03)
        # self.headspace = HeadSpace([0, 0.25], [-0.25, 0.25], [-0.15, 0.25], 0.04)
        self.salience_macon_width = 0.05
        self.salience_discard = 0.01  # saliences below this fraction are discarded computationally
        self.salience_decay_tau = 1.0
        self.noise_decay_tau = 1.0
        self.noise_spatial_sigma = 1.0
        self.noise_magnitude = 0.1
        self.sausage_cen_x_offset = 0.10
        self.sausage_x_min = 0.0
        self.sausage_x_min_margin = 0.1
        self.sausage_rad = 0.25
        self.sausage_heightAboveFloor = 0.15
        self.sausage_var_shell = 0.2
        self.sausage_var_height = 0.25
        self.T_silent_init_period = 0.5
        self.T_silent_recovery = 2.0

        # ior pars
        self.ior_max_age = 30.0  # seconds
        self.ior_sigma = 0.2  # metres
        self.ior_strength = 1.0  # magnitude of inhibition from each visit
        self.ior_spherical = False  # if True, inhibit spherically (rather than cylindrically)

        # contacts pars
        self.contacts_max_age = 30.0  # seconds
        self.contacts_xy_res = 0.03  # maximum resolution at which contacts are stored

        # exclusion reflex
        self.exclusion_gain = 0.25
        self.exclusion_enable = True

        # action pars
        self.pri_idle = 0.25
        self.pri_ongoing = 0.75
        self.thresh_cancel = 0.25
        self.orient_min_time = 0.75
        self.orient_max_speed = 0.1
        self.orient_shimmy_dist = 0.1

        # whisking pars
        self.fW = 2
        self.theta_rcp = -np.pi / 3
        self.theta_min = -np.pi / 4
        self.theta_max = np.pi / 2
        self.theta_spread = np.pi / 36
