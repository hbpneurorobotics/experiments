# this file provides the interface between the generic kc
# object and the system that is using it

from kc import *
from model_pars import *

KC_FRAME_BODY = 0
KC_FRAME_NECK = 1
KC_FRAME_GMBL = 2
KC_FRAME_HEAD = 3

kc_init(0.02, KC_PUSH_FLAG_VELOCITY, KC_FRAME_HEAD)


def kc_whiskeye():
    return KinematicChain([
        ['BODY',
         np.array([0.0, 0.0, 0.0]),
         'z',
         0.0,
         [KC_ANGLE_UNCONSTRAINED, KC_ANGLE_UNCONSTRAINED, KC_ANGLE_UNCONSTRAINED]],
        ['NECK',
         np.array([WHISKY_LOC_LIFT_X, WHISKY_LOC_LIFT_Y, WHISKY_LOC_LIFT_Z]),
         'y',
         WHISKY_LIFT_INI_RAD,
         [WHISKY_LIFT_MIN_RAD, WHISKY_LIFT_MAX_RAD, KC_ANGLE_UNCONSTRAINED]],
        ['GMBL',
         np.array([WHISKY_LOC_PITCH_X, WHISKY_LOC_PITCH_Y, WHISKY_LOC_PITCH_Z]),
         'y',
         WHISKY_PITCH_INI_RAD,
         [WHISKY_PITCH_MIN_RAD, WHISKY_PITCH_MAX_RAD, KC_ANGLE_UNCONSTRAINED]],
        ['HEAD',
         np.array([WHISKY_LOC_YAW_X, WHISKY_LOC_YAW_Y, WHISKY_LOC_YAW_Z]),
         'z',
         WHISKY_YAW_INI_RAD,
         [WHISKY_YAW_MIN_RAD, WHISKY_YAW_MAX_RAD, KC_ANGLE_UNCONSTRAINED]]
    ],
        True)


KC_FRAME_ROWT = 0
KC_FRAME_ROTX = 1
KC_FRAME_ROTY = 2
KC_FRAME_ROW_BOARD = 3
KC_FRAME_WHISKER_BASE = 4
KC_FRAME_WHISKER = 5


def kc_whisker(row, col, row_board, whisker):
    rx = row_board[0]
    ry = row_board[1]
    rz = row_board[2]
    xyz = np.array(row_board[3:6])

    rw = whisker[0]
    whisker_x = whisker[1]
    whisker_y = whisker[2]
    whisker_z = whisker[3]

    initial_theta = 0.0

    return KinematicChain([
        ['ROWT',
         xyz,
         'x', 0, None],
        ['ROTX',
         np.array([0, 0, 0]),
         'x', rx, None],
        ['ROTY',
         np.array([0, 0, 0]),
         'y', ry, None],
        ['ROW_BOARD',
         np.array([0, 0, 0]),
         'z', rz, None],
        ['WHISKER_BASE',
         np.array([whisker_x, whisker_y, whisker_z]),
         'x', rw, None],
        ['WHISKER',
         np.array([0.0, 0.0, 0.0]),
         'y',
         initial_theta,
         [KC_ANGLE_UNCONSTRAINED, KC_ANGLE_UNCONSTRAINED, KC_ANGLE_UNCONSTRAINED]]
    ])


def kc_whiskers(pars):
    kc = []
    for row in pars.rows:
        w = []
        for col in pars.cols:
            w.append(kc_whisker(row, col, pars.row_boards[row], pars.whiskers[col]))
        kc.append(w)
    #### validation ####
    # xyz = np.array([0.0, 0.0, 0.0])
    # xyz = w[0].changeFrameAbs(KC_FRAME_ROW_BOARD, KC_FRAME_PARENT, xyz)
    # print xyz
    #### validation ####
    return kc


def kc_camera2head(camera, pos):
    # Apply divergence
    # -ve once, because azimuth of camera not object
    # -ve again, because we're inverting camera mapping
    # +ve overall
    if camera == 'r':
        azim = WHISKY_CAM_DIVERGENCE
    elif camera == 'l':
        azim = -WHISKY_CAM_DIVERGENCE
    else:
        raise ValueError('camera not r or l ')

    pos = kc_rotate(pos, 'z', azim)

    # Apply elevation
    # As above, two -ves mmake +ve.
    # Camera elevation defined as +ve == upwards
    # Counter to +ve == rotation from z to x RH convention.
    # So end up with a -ve
    elev = WHISKY_CAM_ELEVATION
    pos = kc_rotate(pos, 'y', -elev)

    # Apply translation ( for left camera, y value gets inverted )
    pos[0] += WHISKY_LOC_EYE_X
    if camera == 'r':
        pos[1] += WHISKY_LOC_EYE_Y
    else:
        pos[1] -= WHISKY_LOC_EYE_Y
    pos[2] += WHISKY_LOC_EYE_Z

    return pos
