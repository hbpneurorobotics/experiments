import time

from kc_interf import *
from yaca_support import Perf


class ModuleReflex:

    def __init__(self, state):

        self.state = state
        self.perf = Perf("ModuleReflex")

    def perf(self):

        # performance test
        ti = time.time()
        for i in range(0, 50):
            self.tick()
        perf_report("ModuleReflex", ti)

    def tick(self):

        # performance
        self.perf.tick_i()

        # if enabled
        if self.state.pars.exclusion_enable:

            # get list of contacted locations
            contacts = self.state.sig.contacted_list_WORLD

            # get sensory kc
            kc = self.state.kc_s

            # process each for a push
            for contact in contacts:

                # unpack contact
                x_WORLD = contact[:3]
                w = contact[3]

                # WORLD -> HEAD
                x_HEAD = kc.changeFrameAbs(KC_FRAME_PARENT, KC_FRAME_HEAD, x_WORLD)

                # compute contact distance from head center (in HEAD)
                # we can do this in 2D (to treat obstacles as infinite
                # in vertical extent) or in 3D (to allow poking in below
                # and above them)
                if self.state.pars.exclude_in_3D:
                    v = self.state.pars.center_HEAD - x_HEAD
                else:
                    v = self.state.pars.center_HEAD[:2] - x_HEAD[:2]
                    v = np.concatenate((v, np.array([0.0])))
                r = np.sqrt(np.sum(v ** 2))

                # CHECK:
                """
                center_WORLD = kc.changeFrameAbs(KC_FRAME_HEAD, KC_FRAME_PARENT, self.state.pars.center_HEAD)
                r_check = np.sqrt(np.sum((x_WORLD - center_WORLD) ** 2))
                print "*", r / r_check * 100.0
                """
                # CHECK:

                # get distance from surface
                r_surf = r - self.state.pars.radius_HEAD

                # clip distance from surface to exclusion zone
                d_surf = np.clip(r_surf, 0.0, self.state.pars.exclusion_dist_HEAD)

                # invert so that it is large when touching the surface, zero at the exclusion zone's edge
                mag = self.state.pars.exclusion_dist_HEAD - d_surf

                # debug
                # print "?", x_WORLD, x_HEAD, self.state.pars.center_HEAD, r, r_surf, d_surf, mag

                # if non-zero, apply a push
                if mag > 0.0:
                    # push vector is unit vector from contact towards center
                    u = v / np.linalg.norm(v)

                    # weighted by exclusion impulse and contact confidence (macon)
                    gain = mag * w
                    pushvec = u * gain

                    # apply push to center of HEAD
                    push = KinematicPush('reflex')
                    push.flags = KC_PUSH_FLAG_IMPULSE
                    push.pos = self.state.pars.center_HEAD
                    push.vec = pushvec * self.state.pars.exclusion_gain

                    # debug
                    # print ">>>>>>", w, gain, np.linalg.norm(push.vec), push.vec

                    # send push
                    self.state.sig.pushes.append(push)

                    # log push magnitude
                    # self.state.logwrite(np.linalg.norm(pushvec))

        # performance
        self.perf.tick_f()
