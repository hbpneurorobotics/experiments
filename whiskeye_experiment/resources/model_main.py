#!/usr/bin/python

# import ROS
import rospy
import std_msgs
from ModuleAction import *
from ModuleMotor import *
from ModulePlatform import *
from ModuleReflex import *
from ModuleSensory import *
from ModuleSpatial import *
from ModuleWorld import *

from model_definition import *


def main():
    # get state
    state = SystemState()

    # create modules
    m_sensory = ModuleSensory(state)
    m_spatial = ModuleSpatial(state)
    m_action = ModuleAction(state)
    m_reflex = ModuleReflex(state)
    m_world = ModuleWorld(state)
    m_motor = ModuleMotor(state)

    # performance
    if False:
        print("---- perf")
        m_sensory.perf()
        m_spatial.perf()
        m_action.perf()
        m_reflex.perf()
        m_world.perf()
        m_motor.perf()
        return

    # validation
    # m_sensory.validate()

    # init node
    rospy.init_node('model')

    # publish
    topic = state.pars.topic_root + "/tick"
    print("publishing", topic)
    pub_tick = rospy.Publisher(topic, std_msgs.msg.Int32, queue_size=0)

    # use trace file
    # state.tracefile = 'record'
    # state.tracefile = '/tmp/yacatrace.1554395598'

    # create modules
    m_platform = ModulePlatform(state)

    # for each tick
    while not state.cancel:

        # publish tick
        pub_tick.publish(state.time_n)

        # tick modules
        if not m_platform.tick():
            break
        # if n == 200:
        #	m_sensory.perf_breakdown()
        #	break
        m_sensory.tick()
        m_spatial.tick()
        m_action.tick()
        m_reflex.tick()
        m_world.tick()
        m_motor.tick()

        # tick state
        state.tick()

    # serialize input
    m_platform.term()

    # debug
    print("\n\n[exit dump]")
    print("state.sig.contacted_list_WORLD")
    print(state.sig.contacted_list_WORLD)
    print("state.sig.head_center_WORLD")
    print(state.sig.head_center_WORLD)

    # print dir(state.sig)


# cProfile.run('main()', sort='cumtime')
main()
