import copy
import time

from scipy.ndimage import gaussian_filter

from kc_interf import *
from yaca_support import *


class ModuleSpatial:

    def __init__(self, state):

        self.state = state
        self.perf = Perf("ModuleSpatial")

        # precompute
        self.salience_macon_width_recip = 1.0 / self.state.pars.salience_macon_width

        # create spatial map
        [self.mappos, self.mapval] = self.state.pars.headspace.createMap()

        # mappos is in HEAD - we dynamically maintain a version in WORLD
        # for use with some types of injected signal
        self.mappos_WORLD = None

        # create spatial noise
        self.mapnoise = np.zeros(self.mapval.shape)

        # precompute spatial noise weighting as a portion of the surface
        # of a sphere centred on the origin of HEAD
        cen = copy.copy(self.state.pars.fovea_HEAD)
        cen[0] -= self.state.pars.sausage_cen_x_offset
        self.sausage_template = np.zeros(self.mapval.shape)
        shape = self.state.pars.headspace.shape
        for xi in range(0, shape[0]):
            for yi in range(0, shape[1]):
                for zi in range(0, shape[2]):
                    q = self.mappos[xi][yi][zi]
                    d = q - cen
                    d[
                        2] = 0  # ignore height, we'll do height above floor using run-time robot configuration
                    r = np.sqrt((d * d).sum())  # radius of point from sphere centre
                    t = (r - self.state.pars.sausage_rad) / self.state.pars.sausage_var_shell
                    bias_wall = np.exp(-t * t)
                    forward = (q[
                                   0] - self.state.pars.sausage_x_min) / self.state.pars.sausage_x_min_margin
                    bias_forward = np.maximum(np.minimum(forward, 1.0), 0.0)
                    mag = bias_wall * bias_forward
                    self.sausage_template[xi][yi][zi] = mag

        # normalise
        # self.sausage_template /= self.state.pars.sausage_min_fwd

        # pre-apply pars.noise_magnitude
        self.sausage_template *= self.state.pars.noise_magnitude

        # pre-compute exponential "range" in units of map cells
        for i in range(0, 100):
            x = self.state.pars.headspace.step * (i + 1)
            q = x / self.state.pars.salience_macon_width
            z = math.exp(-q * q)
            if z < self.state.pars.salience_discard:
                self.headspace_L = i
                break

        # pre-compute time parameters
        self.salience_decay_lambda = tau2lambda(self.state.pars.salience_decay_tau,
                                                self.state.pars.fS_lo)
        self.noise_decay_lambda = tau2lambda(self.state.pars.noise_decay_tau, self.state.pars.fS_lo)

        # silent gate
        self.t_silent = None
        self.t_silent_recovery = 0.0

    def remap(self, mapslice):

        return np.transpose(mapslice)

    def perf(self):

        # performance test
        ti = time.time()
        for i in range(0, 50):
            self.tick()
        perf_report("ModuleSpatial", ti)

    def compute_mappos_WORLD(self):

        # get sensory kc
        kc = self.state.kc_s

        # get reference point in one corner
        h0 = self.mappos[0, 0, 0, :]
        w0 = kc.changeFrameAbs(KC_FRAME_HEAD, KC_FRAME_PARENT, h0)

        # get unit vector along axis
        hx = self.mappos[1, 0, 0, :]
        wx = kc.changeFrameAbs(KC_FRAME_HEAD, KC_FRAME_PARENT, hx)
        ux = wx - w0

        # get unit vector along axis
        hy = self.mappos[0, 1, 0, :]
        wy = kc.changeFrameAbs(KC_FRAME_HEAD, KC_FRAME_PARENT, hy)
        uy = wy - w0

        # get unit vector along axis
        hz = self.mappos[0, 0, 1, :]
        wz = kc.changeFrameAbs(KC_FRAME_HEAD, KC_FRAME_PARENT, hz)
        uz = wz - w0

        # use unit vectors to fill mappos_WORLD
        # this imp is very slow - the one below was created to be
        # quite a lot faster, like about 30 times faster I think
        """
        self.mappos_WORLD = copy.copy(self.mappos)
        for x in range(self.mappos.shape[0]):
            wx = w0 + x * ux
            for y in range(self.mappos.shape[1]):
                wxy = wx + y * uy
                for z in range(self.mappos.shape[2]):
                    wxyz = wxy + z * uz
                    self.mappos_WORLD[x, y, z, :] = wxyz
                    """

        # extract dimensions
        nx = self.mappos.shape[0]
        ny = self.mappos.shape[1]
        nz = self.mappos.shape[2]

        # expand x
        xx = np.array(list(range(nx))) * ux[0]
        xy = np.array(list(range(ny))) * uy[0]
        xz = np.array(list(range(nz))) * uz[0]
        xx_ = np.tile(xx[:, np.newaxis, np.newaxis], (1, ny, nz))
        xy_ = np.tile(xy[np.newaxis, :, np.newaxis], (nx, 1, nz))
        xz_ = np.tile(xz[np.newaxis, np.newaxis, :], (nx, ny, 1))
        x = xx_ + xy_ + xz_ + w0[0]

        # expand y
        yx = np.array(list(range(nx))) * ux[1]
        yy = np.array(list(range(ny))) * uy[1]
        yz = np.array(list(range(nz))) * uz[1]
        yx_ = np.tile(yx[:, np.newaxis, np.newaxis], (1, ny, nz))
        yy_ = np.tile(yy[np.newaxis, :, np.newaxis], (nx, 1, nz))
        yz_ = np.tile(yz[np.newaxis, np.newaxis, :], (nx, ny, 1))
        y = yx_ + yy_ + yz_ + w0[1]

        # expand z
        zx = np.array(list(range(nx))) * ux[2]
        zy = np.array(list(range(ny))) * uy[2]
        zz = np.array(list(range(nz))) * uz[2]
        zx_ = np.tile(zx[:, np.newaxis, np.newaxis], (1, ny, nz))
        zy_ = np.tile(zy[np.newaxis, :, np.newaxis], (nx, 1, nz))
        zz_ = np.tile(zz[np.newaxis, np.newaxis, :], (nx, ny, 1))
        z = zx_ + zy_ + zz_ + w0[2]

        # combine
        xyz = np.stack([x, y, z], axis=3)
        # d = (self.mappos_WORLD - xyz).flatten()
        # print np.max(np.abs(d))
        self.mappos_WORLD = xyz

    def tick(self):

        # performance
        self.perf.tick_i()

        # get headspace mappos(_HEAD) in WORLD
        self.compute_mappos_WORLD()

        # performance
        self.perf.tick_m()

        # get sensory kc
        kc = self.state.kc_s

        #### COMPUTE IOR ####

        # do suppression by IOR
        self.ior()

        # performance
        self.perf.tick_m()

        #### COMPUTE SAUSAGE ####

        # recover headspace map of absolute height above floor (HAF)
        heightAboveFloor = self.mappos_WORLD[:, :, :, 2]

        # subtract from that the preferred HAF of the sausage, so 0 is now the preferred sausage HAF
        heightAboveFloor -= self.state.pars.sausage_heightAboveFloor

        # compute a 3D weight matrix that favours that zero point in relative HAF
        weight_var = heightAboveFloor / self.state.pars.sausage_var_height
        sausage_z_bias = np.exp(-weight_var * weight_var)

        # merge that weight based on HAF with pre-computed sausage template
        # which encodes the sausage shape in x/y
        self.state.sig.sausage = sausage_z_bias * self.sausage_template

        #### DO SILENT GATING ####

        # do silent gating - if the active action has marked "silent"
        # then we do not propagate noise, and on the first sample of
        # "silent" we also clear the historical data

        # assume normal weighting of whisker contacts, unless silent
        weight_contact = 1.0

        # if orienting
        if self.state.sig.orienting:

            # if not previously in silent
            if self.t_silent is None:
                # set dt to -ve the silencing period
                self.t_silent = self.state.time_t + self.state.pars.T_silent_init_period

            # and reset the recovery timer
            self.t_silent_recovery = self.state.time_t

            # handle silent gating
            if self.state.time_t < self.t_silent:
                # set whisker contact weighting to zero
                weight_contact = 0.0

                # clear historical salience state
                self.mapval *= 0.0

        # otherwise
        else:

            # clear the silent state
            self.t_silent = None

        # noise weight recovers after silent period through "recovery"
        dt = self.state.time_t - self.t_silent_recovery
        weight_silence_recovery = np.clip(dt / self.state.pars.T_silent_recovery, 0.0, 1.0)

        # performance
        self.perf.tick_m()

        #### APPLY NOISE MODULATIONS ####

        # apply ior squeezing so that if the usual sausage locations
        # are all IOR'd, other regions of the map become noisier so
        # that the robot does /something/ even if the sausage options
        # look unappealing
        a = np.sum(self.state.sig.sausage)
        b = np.sum(self.state.sig.sausage * self.state.sig.ior)
        gain = a / b

        # apply squeezing gain
        sausage_squeezed = self.state.sig.sausage * gain

        # debug out
        self.state.sig.sausage_squeezed = sausage_squeezed * self.state.sig.ior * 5.0

        # apply noise silencing weight
        sausage_squeezed_silenced = sausage_squeezed * weight_silence_recovery

        #### COMPUTE AND INJECT NOISE ####

        # generate a uniform noise signal with appropriate temporal
        # and spatial characteristics
        self.mapnoise += np.random.standard_normal(self.mapval.shape)
        self.mapnoise = gaussian_filter(self.mapnoise, self.state.pars.noise_spatial_sigma)
        self.mapnoise *= self.noise_decay_lambda

        # now that we have the overall weight matrix, and a uniform noise
        # signal with appropriate characteristics, we weight the latter by
        # the former to give the actual injection of noise to the map
        self.mapval += self.mapnoise * sausage_squeezed_silenced

        # performance
        self.perf.tick_m()

        #### COMPUTE DYNAMICS ####

        # dynamics
        # self.mapval = scipy.ndimage.filters.convolve(self.mapval, self.mapfilt)
        # self.mapval = gaussian_filter(self.mapval, 2.0)
        self.mapval *= self.salience_decay_lambda

        #### INJECT CONTACT ####

        # for each whisker
        for row in self.state.pars.rows:
            for col in self.state.pars.cols:

                # get macon
                macon = self.state.sig.macon_lo[row][col]
                w = macon * weight_contact
                if w == 0.0:
                    continue

                # inject energy at the whisker tip, magnitude from macon
                pos = self.state.sig.whisker_tip_pos[row][col]
                self.inject(pos, w)

                # also store this for adding to the world map
                pos_WORLD = kc.changeFrameAbs(KC_FRAME_HEAD, KC_FRAME_PARENT, pos)
                contact = [pos_WORLD, w]
                self.state.sig.contacts.append(contact)

        #### APPLY IOR ####

        # apply IOR to salience map
        self.mapval_inh = self.mapval * self.state.sig.ior

        # report total activity
        # print np.sum(self.mapval_inh)

        # performance
        self.perf.tick_m()

        #### COMPUTE SUMMARY ####

        # compute threshold
        thresh = np.max(self.mapval_inh) * 0.2

        # compute summary
        # thresh = 0.2
        over_thresh = (self.mapval_inh - thresh).clip(min=0)
        over_thresh_4d = np.tile(np.expand_dims(over_thresh, 4), (1, 1, 1, 3))
        # q = over_thresh_4d.shape
        self.state.sig.focus.mag = np.sum(over_thresh)
        self.state.sig.focus.max = np.max(over_thresh)
        if self.state.sig.focus.mag > 0:
            self.state.sig.focus.pos = np.sum(over_thresh_4d * self.mappos,
                                              axis=(0, 1, 2)) / self.state.sig.focus.mag
        else:
            self.state.sig.focus.pos = np.array([0, 0, 0])

        #		print self.state.sig.focus.mag, self.state.sig.focus.max

        # debug (show active rows)
        """
        mn = []
        for row in self.state.pars.rows:
            mn.append(int(np.mean(self.state.sig.macon_lo[row])*100))
        print mn
        """

        #### FINALISE ####

        # publish
        self.state.sig.mapval = self.mapval_inh

        # performance
        self.perf.tick_f()

    def ior(self):

        # get list of recently visited locations in WORLD
        locs = self.state.sig.visited_list_WORLD

        # get default weighting (no inhib, unity weight)
        w = np.ones(self.mapval.shape)

        # get pars
        sigma_recip = 1.0 / self.state.pars.ior_sigma
        strength = self.state.pars.ior_strength
        spherical = self.state.pars.ior_spherical

        # calculate d threshold to skip exp() function
        eps = 0.05  # fractional error that is acceptable to skip (1.0 is peak)
        d_max = np.sqrt(-np.log(eps)) / sigma_recip

        # cylindrical mode pre-computation
        if not spherical:
            # get x/y projection of headspace (i.e. projection onto floor)
            mappos_xy_WORLD = self.mappos_WORLD[:, :, :, 0:2]

        # for each recently visited location
        for loc in locs:

            # inhibit a spherical (3D gaussian) region in headspace
            if spherical:

                # not implemented yet
                pass

            # inhibit a cylindrical (2D gaussian) region oriented vertically in WORLD passing through headspace
            else:

                # compute response
                sh = mappos_xy_WORLD.shape
                loc_xy = np.tile(loc[0:2], (sh[0], sh[1], sh[2], 1))
                dxy = loc_xy - mappos_xy_WORLD
                d = np.sqrt(np.sum(dxy ** 2, 3))
                d *= (-sigma_recip)
                r = np.exp(d)

                # apply
                w -= r * strength

        # constrain to 0.0
        w = np.clip(w, 0.0, 1.0)

        # store for monitoring
        self.state.sig.ior = w

    def inject(self, pos, mag):

        # run dynamics
        lam = self.salience_decay_lambda

        # get range of headspace in which inject might have non-negligible effect
        [xii, yii, zii] = self.state.pars.headspace.getRanges(pos, self.headspace_L)

        # create map indexing object for that range
        ind = np.ix_(xii, yii, zii)

        # inject into that range
        mappos = self.mappos[ind]
        dpos = pos - mappos
        x = np.linalg.norm(dpos, axis=3)
        q = x * self.salience_macon_width_recip
        q2 = -q * q
        z = np.exp(q2) * mag
        self.mapval[ind] += z

        """
        # for each cell in that range
        for xi in xii:
            for yi in yii:
                for zi in zii:

                    # compute exponential
                    mappos = self.mappos[xi, yi, zi]
                    dpos = pos - mappos
                    x = np.linalg.norm(dpos)
                    q = x / self.state.pars.salience_macon_width
                    z = math.exp(-q*q)

                    # inject using magnitude
                    self.mapval[xi, yi, zi] += z * mag
        """
