import pickle

# import ROS
import rospy
import std_msgs
from geometry_msgs.msg import *
from sensor_msgs.msg import *

from whiskeye_plugin.msg import Bridge_u
from yaca_support import *


def set_multi_array_dim_from_example(dim, x, labels=None):
    n = len(x.shape)
    for i in range(0, n):
        d = std_msgs.msg.MultiArrayDimension()
        d.size = x.shape[i]
        d.stride = x.strides[i]
        if labels is not None:
            d.label = labels[i]
        dim.append(d)


class InputData:

    def __init__(self):
        self.neck = None
        self.theta = None
        self.xy = None
        self.is_physical = None
        self.is_bumper = None
        self.cmd_vel_gui = None

    def is_ready(self):
        return not (self.neck is None)


class InputInterfaceTrace:

    def __init__(self, state):

        # store global state
        self.state = state

        # empty input data object
        self.input = InputData()

        # load trace file
        with open(self.state.tracefile, "rb") as file:
            self.trace = pickle.load(file)

        # set counter
        self.i = 0

    def read(self):

        # check for shutdown
        if rospy.is_shutdown():
            return None

        # if run out, return None
        if self.i == len(self.trace):
            return None

        # otherwise return and count
        self.i += 1
        return self.trace[self.i - 1]


class InputInterfaceROS:

    def __init__(self, state, topic_root):

        # store global state
        self.state = state
        self.camera_frame_arrived = False

        # empty input data object
        self.input = InputData()

        # subscribe
        topic = topic_root + '/body/bumper'
        print("subscribing", topic)
        self.sub_bumper = rospy.Subscriber(topic, std_msgs.msg.Bool, self.callback_bumper,
                                           queue_size=1, tcp_nodelay=True)

        # subscribe
        topic = topic_root + '/head/bridge_u'
        print("subscribing", topic)
        self.sub_bridge_u = rospy.Subscriber(topic, Bridge_u, self.callback_bridge_u, queue_size=1,
                                             tcp_nodelay=True)

        # subscribe
        topic = topic_root + '/gui/cmd_vel'
        print("subscribing", topic)
        self.sub_cmd_vel = rospy.Subscriber(topic, Twist, self.callback_cmd_vel, queue_size=1,
                                            tcp_nodelay=True)

        # subscribe
        topic = topic_root + '/head/cam0/image_raw/compressed'
        print("subscribing", topic)
        self.sub_cam0 = rospy.Subscriber(topic, CompressedImage, self.callback_cam0, queue_size=1,
                                         tcp_nodelay=True)

    def read(self):

        # wait for input
        while not self.input.is_ready():

            # check for shutdown
            if rospy.is_shutdown():
                return None

            # else sleep until input arrives
            time.sleep(0.001)

        # get input
        input = self.input

        # sync with clock and clear local buffers
        self.input = InputData()

        # ok
        return input

    def callback_cam0(self, msg):

        self.camera_frame_arrived = True

    def callback_bumper(self, msg):

        if msg.data:
            print("detected bumper press")
            self.input.is_bumper = True
            self.state.cancel = True

    def callback_bridge_u(self, msg):

        """
        t = time.time()
        dt = t - self.t_bak
        print np.round(dt * 1000)
        self.t_bak = t
        """

        x = np.asarray(msg.theta.data)
        x = np.reshape(x, self.state.pars.shape_hi)
        self.input.theta = x

        x = np.asarray(msg.xy.data)
        x = np.reshape(x, self.state.pars.shape_hi_2)
        self.input.xy = x

        x = np.asarray(msg.neck.data)
        x = np.reshape(x, (self.state.pars.fS_scale, 3))
        self.input.neck = np.mean(x, axis=0)

        self.input.is_physical = msg.physical.data

    def callback_cmd_vel(self, msg):

        self.input.cmd_vel_gui = [msg.linear.x, msg.linear.y, msg.angular.z]


class ModulePlatform:

    def __init__(self, state):

        # init random
        np.random.seed(42)

        # store global state
        self.state = state

        # create local state
        self.trace = []
        self.t_bak = 0
        topic_root = '/whiskeye'

        # GP log
        self.log = np.array([])
        self.do_log = True

        # create input interface
        if self.state.tracefile is None or self.state.tracefile == 'record':
            self.input_interf = InputInterfaceROS(state, topic_root)
        else:
            self.input_interf = InputInterfaceTrace(state)

        # publish
        topic = topic_root + '/head/neck_cmd'
        print("publishing", topic)
        self.pub_neck_cmd = rospy.Publisher(topic, std_msgs.msg.Float32MultiArray, queue_size=0)

        # publish
        topic = topic_root + '/body/cmd_vel'
        print("publishing", topic)
        self.pub_cmd_vel = rospy.Publisher(topic, Twist, queue_size=0)

        # publish
        topic = topic_root + '/head/theta_cmd'
        print("publishing", topic)
        self.pub_theta_cmd = rospy.Publisher(topic, std_msgs.msg.Float32MultiArray, queue_size=0)

        # publish
        topic = self.state.pars.topic_root + "/max_macon"
        print("publishing", topic)
        self.pub_max_macon = rospy.Publisher(topic, std_msgs.msg.UInt8, queue_size=0)

        # publish
        topic = self.state.pars.topic_root + "/protracting"
        print("publishing", topic)
        self.pub_protracting = rospy.Publisher(topic, std_msgs.msg.Int8, queue_size=0)

        # publish
        topic = self.state.pars.topic_root + "/mapval"
        print("publishing", topic)
        self.pub_mapval = rospy.Publisher(topic, std_msgs.msg.Float32MultiArray, queue_size=0)

        # publish
        topic = self.state.pars.topic_root + "/mapval3D"
        print("publishing", topic)
        self.pub_mapval3D = rospy.Publisher(topic, std_msgs.msg.Float32MultiArray, queue_size=0)

        # publish
        topic = self.state.pars.topic_root + "/ior"
        print("publishing", topic)
        self.pub_ior = rospy.Publisher(topic, std_msgs.msg.Float32MultiArray, queue_size=0)

        # publish
        topic = self.state.pars.topic_root + "/sausage_squeezed"
        print("publishing", topic)
        self.pub_sausage_squeezed = rospy.Publisher(topic, std_msgs.msg.Float32MultiArray,
                                                    queue_size=0)

        # publish
        topic = self.state.pars.topic_root + "/xy"
        print("publishing", topic)
        self.pub_xy = rospy.Publisher(topic, std_msgs.msg.Float32MultiArray, queue_size=0)

        # publish
        topic = self.state.pars.topic_root + "/xy_q"
        print("publishing", topic)
        self.pub_xy_q = rospy.Publisher(topic, std_msgs.msg.Float32MultiArray, queue_size=0)

        # publish
        topic = self.state.pars.topic_root + "/theta"
        print("publishing", topic)
        self.pub_theta = rospy.Publisher(topic, std_msgs.msg.Float32MultiArray, queue_size=0)

        # publish
        topic = self.state.pars.topic_root + "/theta_f"
        print("publishing", topic)
        self.pub_theta_f = rospy.Publisher(topic, std_msgs.msg.Float32MultiArray, queue_size=0)

        # publish
        topic = self.state.pars.topic_root + "/visited"
        print("publishing", topic)
        self.pub_visited = rospy.Publisher(topic, std_msgs.msg.Float32MultiArray, queue_size=0)

        # publish
        topic = self.state.pars.topic_root + "/contacted"
        print("publishing", topic)
        self.pub_contacted = rospy.Publisher(topic, std_msgs.msg.Float32MultiArray, queue_size=0)

        # publish
        topic = self.state.pars.topic_root + "/head_center_WORLD"
        print("publishing", topic)
        self.pub_head_center_WORLD = rospy.Publisher(topic, std_msgs.msg.Float32MultiArray,
                                                     queue_size=0)

        # publish final marker topic to show publishing is complete
        topic = self.state.pars.topic_root + "/fin"
        print("publishing", topic)
        self.pub_fin = rospy.Publisher(topic, std_msgs.msg.Int32, queue_size=0)

    def publish_mapval_debug(self, mapval, pub, threed=False):

        if mapval is not None:

            # ensure float32
            mapval = mapval.astype('float32')

            if threed:

                # 3D
                msg = std_msgs.msg.Float32MultiArray()
                set_multi_array_dim_from_example(msg.layout.dim, mapval, ['x', 'y', 'z'])
                msg.data = mapval.flatten().tolist()
                pub.publish(msg)

            else:

                # 2D
                slice = mapval[:, :, self.state.pars.headspace.zi_cen]
                msg = std_msgs.msg.Float32MultiArray()
                set_multi_array_dim_from_example(msg.layout.dim, slice, ['x', 'y'])
                msg.data = slice.flatten().tolist()
                pub.publish(msg)

    def tick(self):

        # retrieve input object
        self.state.sig.input = self.input_interf.read()

        # if that's None, we're done
        if self.state.sig.input is None:
            return False

        # if recording
        if self.state.tracefile == 'record':
            # store input for serialization
            self.trace.append(self.state.sig.input)

        # publish
        msg = Twist()
        msg.linear.x = self.state.sig.cmd_vel[0]
        msg.linear.y = self.state.sig.cmd_vel[1]
        msg.angular.z = self.state.sig.cmd_vel[2]
        self.pub_cmd_vel.publish(msg)
        # print("x")

        # publish
        msg = std_msgs.msg.Float32MultiArray()
        set_multi_array_dim_from_example(msg.layout.dim, self.state.sig.neck_cmd, ['dof'])
        msg.data = self.state.sig.neck_cmd.flatten().tolist()
        self.pub_neck_cmd.publish(msg)

        # debug
        out_neck = msg.data

        # publish
        msg = std_msgs.msg.Float32MultiArray()
        set_multi_array_dim_from_example(msg.layout.dim, self.state.sig.theta_cmd, ['row', 'col'])
        msg.data = self.state.sig.theta_cmd.flatten().tolist()
        self.pub_theta_cmd.publish(msg)

        # debug
        out_theta = msg.data

        # debug
        if self.do_log:
            out = np.concatenate((out_neck, out_theta))[:, np.newaxis].T
            if self.log.size == 0:
                self.log = out
            else:
                self.log = np.append(self.log, out, axis=0)

        # publish
        msg = std_msgs.msg.Int8()
        if self.state.sig.protracting:
            msg.data = 1
        self.pub_protracting.publish(msg)

        # publish
        msg = std_msgs.msg.UInt8()
        msg.data = (np.max(self.state.sig.macon_lo) * 255).astype(np.uint8)
        self.pub_max_macon.publish(msg)

        # publish debug
        self.publish_mapval_debug(self.state.sig.mapval, self.pub_mapval)
        if self.input_interf.camera_frame_arrived:
            self.input_interf.camera_frame_arrived = False
            self.publish_mapval_debug(self.state.sig.mapval, self.pub_mapval3D, True)
        self.publish_mapval_debug(self.state.sig.ior, self.pub_ior)
        self.publish_mapval_debug(self.state.sig.sausage_squeezed, self.pub_sausage_squeezed)

        # publish debugs
        if True:
            # republish inputs for matlab

            # xy
            msg = std_msgs.msg.Float32MultiArray()
            msg.data = self.state.sig.input.xy.flatten().tolist()
            self.pub_xy.publish(msg)
            # xy_q
            msg = std_msgs.msg.Float32MultiArray()
            msg.data = self.state.sig.xy_q.flatten().tolist()
            self.pub_xy_q.publish(msg)
            # theta
            msg = std_msgs.msg.Float32MultiArray()
            msg.data = self.state.sig.input.theta.flatten().tolist()
            self.pub_theta.publish(msg)
            # theta_f
            msg = std_msgs.msg.Float32MultiArray()
            msg.data = self.state.sig.theta_f.flatten().tolist()
            self.pub_theta_f.publish(msg)
            # visited
            msg = std_msgs.msg.Float32MultiArray()
            x = self.state.sig.visited_list_WORLD
            if len(x) > 0:
                msg.data = np.vstack(x).flatten().tolist()
            self.pub_visited.publish(msg)
            # contacted
            msg = std_msgs.msg.Float32MultiArray()
            x = self.state.sig.contacted_list_WORLD
            if len(x) > 0:
                msg.data = np.vstack(x).flatten().tolist()
            self.pub_contacted.publish(msg)
            # head_center_WORLD
            msg = std_msgs.msg.Float32MultiArray()
            x = self.state.sig.head_center_WORLD
            if x is not None:
                msg.data = x.flatten().tolist()
                self.pub_head_center_WORLD.publish(msg)

        # ok to continue
        return True

    def term(self):

        # if recording
        if self.state.tracefile == 'record':
            t = time.time()
            filename = "/tmp/yacatrace." + str(int(t))
            with open(filename, "wb") as file_:
                pickle.dump(self.trace, file_)
            print("traced to", filename)

        # if gp log
        if self.do_log:
            np.savetxt("/tmp/log", self.log, fmt='%.5g')
            """
            print "<self.log>"
            sz = self.log.shape
            for i in range(0, sz[0]):
                row = self.log[i, :]
                print row
            print "</self.log>"
            """
