import rospy
import std_msgs

from model_definition import SystemState
from module_platform import ModulePlatform
from module_sensory import ModuleSensory
from module_spatial import ModuleSpatial
from module_action import ModuleAction
from module_reflex import ModuleReflex
from module_world import ModuleWorld
from module_motor import ModuleMotor

# import cProfile
# import camera_filter

system_state = SystemState()
topic = system_state.pars.topic_root + "/tick"


@nrp.MapVariable("m_sensory", initial_value=ModuleSensory(system_state))
@nrp.MapVariable("m_spatial", initial_value=ModuleSpatial(system_state))
@nrp.MapVariable("m_action", initial_value=ModuleAction(system_state))
@nrp.MapVariable("m_reflex", initial_value=ModuleReflex(system_state))
@nrp.MapVariable("m_world", initial_value=ModuleWorld(system_state))
@nrp.MapVariable("m_motor", initial_value=ModuleMotor(system_state))
@nrp.MapVariable("state", initial_value=system_state)
@nrp.MapVariable("m_platform", initial_value=ModulePlatform(system_state))
@nrp.MapVariable("pub_tick", initial_value=rospy.Publisher(topic, std_msgs.msg.Int32, queue_size=0))
@nrp.Robot2Neuron()
def cog_arch_tf(t, m_sensory, m_spatial, m_action, m_reflex, m_world, m_motor, state, m_platform,
                pub_tick):
    clientLogger.info("Tick all modules")

    m_platform.value.tick()
    # clientLogger.info("m_platform")

    m_sensory.value.tick()
    # clientLogger.info("m_sensory")

    m_spatial.value.tick()
    # clientLogger.info("m_spatial")

    m_action.value.tick()
    # clientLogger.info("m_action")

    m_reflex.value.tick()
    # clientLogger.info("m_reflex")

    m_world.value.tick()
    # clientLogger.info("m_world")

    m_motor.value.tick()
    # clientLogger.info("m_motor")

    state.value.tick()
    # clientLogger.info("state")
