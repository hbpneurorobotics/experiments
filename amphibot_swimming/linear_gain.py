"""
This module contains the transfer function which is responsible for applying the linear gain
to the output of the nengo brain
"""
@nrp.MapRobotPublisher('lamprey_targets',Topic('/lamprey_targets', gazebo_msgs.msg.JointsPositions))
@nrp.Neuron2Robot()
def linear_gain (t,lamprey_targets):
    """
    The transfer function which calculates the target position for salamander joints.

    :param t: the current simulation time
    :param out: connection to the 11 dimensional output signal of the Nengo brain
    :return: a gazebo_msgs/JointPositions message setting the target position for the salamander joints.
    """
    msg = gazebo_msgs.msg.JointsPositions()
    msg.names = ''
    msg.positions = [0]
    lamprey_targets.send_message(msg)