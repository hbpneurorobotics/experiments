# Let the PyBullet Ant Walk

To let the Ant walk you can use the predefined transfer function. This requires however
a TensorFlow virtual environment. The next section describes how to create one.

# Create the TensorFlow virtual environment

First make sure to have installed the package to create a virtual environment and Python 2.
If you have installed the NRP from source this should have already be done.

- Run `virtualenv --python=/usr/bin/python2.7 ~/.opt/tensorflow_venv`

- Run `source ~/.opt/tensorflow_venv/bin/activate`

Only version 1.4.0 of TensorFlow worked for me in combination with the NRP and the
platform_venv so install it:

- Run `pip install tensorflow==1.4.0`

- Close the shell

# Run the transfer function

In the transfer function the virtual environment is loaded (If you install it in another directory make sure to edit the
path accordingly). So to run the transfer function simply press start.