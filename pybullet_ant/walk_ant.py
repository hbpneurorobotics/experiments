@nrp.MapVariable("timesteps", initial_value=0)
@nrp.MapRobotPublisher("set_max_step_size", Topic("/pybullet_ant/set_max_step_size", std_msgs.msg.Float64))
@nrp.MapRobotSubscriber("observation", Topic("/pybullet_ant/observation", std_msgs.msg.Float64MultiArray))
@nrp.MapRobotPublisher("step_simulation", Topic("/pybullet_ant/step_simulation", std_msgs.msg.Bool))
@nrp.MapRobotPublisher("publish_observation", Topic("/pybullet_ant/publish_observation", std_msgs.msg.Bool))
@nrp.MapRobotPublisher("force_joints", Topic("/pybullet_ant/force_all/cmd_force", std_msgs.msg.Float64MultiArray))
@nrp.Robot2Neuron()
def walk_ant(t, observation, step_simulation, publish_observation, force_joints, set_max_step_size, timesteps):
    max_timesteps = 5000

    if timesteps.value < max_timesteps:
        import numpy as np
        import os
        import tensorflow as tf
        import time
        import es_custom_layers as escl

        custom_objects = {"Normc_initializer": escl.Normc_initializer,
                          "ObservationNormalizationLayer": escl.ObservationNormalizationLayer,
                          "DiscretizeActionsUniformLayer": escl.DiscretizeActionsUniformLayer}

        tf.keras.backend.clear_session()
        model = tf.keras.models.load_model("resources/trained_ant_model.h5", custom_objects=custom_objects)

        set_max_step_size.send_message(std_msgs.msg.Float64(data=0.004166666666666667))

        clientLogger.info("Starting walking loop!")
        while timesteps.value < max_timesteps:
            publish_observation.send_message(std_msgs.msg.Bool(data=True))
            sleep = 0.005
            while not observation.value:
                time.sleep(0.005)
                clientLogger.info("Retrying getting observation")
                sleep *= 2
                if sleep > 5.0:
                    break
            if sleep > 5.0:
                break

            current_ob = np.array(observation.value.data)
            ac = model.predict_on_batch(current_ob[None])
            ac = ac[0]
            force_joints.send_message(std_msgs.msg.Float64MultiArray(data=ac))

            # Without sleep the simulation would be too fast
            time.sleep(0.005)
            step_simulation.send_message(std_msgs.msg.Bool(data=True))

            timesteps.value = timesteps.value + 1
        clientLogger.info("Finished walking loop!")
