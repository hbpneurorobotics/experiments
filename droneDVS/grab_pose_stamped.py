from geometry_msgs.msg import PoseStamped, TwistStamped
from nav_msgs.msg import Odometry

'''
Transfer function converting odometry to posestamped, twist, in order to have lighter messages
'''


@nrp.MapRobotPublisher('position', Topic('/quadrotor/pose', PoseStamped))
@nrp.MapRobotPublisher("twist", Topic('/quadrotor/twist', TwistStamped))
@nrp.MapRobotSubscriber("odom", Topic('/quadrotor/ground_truth/state', Odometry))
@nrp.Neuron2Robot()
def grab_pose_stamped(t, odom, twist, position):

    if (msg_odom := odom.value) is None:
        return

    from geometry_msgs.msg import PoseStamped, TwistStamped

    position.send_message(PoseStamped(header=msg_odom.header,
                                      pose=msg_odom.pose.pose))

    twist.send_message(TwistStamped(header=msg_odom.header,
                                    twist=msg_odom.twist.twist))
