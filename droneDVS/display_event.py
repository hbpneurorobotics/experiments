from dvs_msgs.msg import EventArray


@nrp.MapRobotSubscriber("dvs", Topic('/quadrotor/dvs/events', EventArray))
@nrp.Neuron2Robot(Topic('/dvs_rendered_full', sensor_msgs.msg.Image))
def display_event(t, dvs):

    if (event_msg := dvs.value) is None:
        return

    # first dim is the height
    rendered_img = np.ones((128, 128, 3), dtype=np.uint8)

    for event in event_msg.events:
        rendered_img[event.y][event.x] = (event.polarity * 255, 255, 0)

    return CvBridge().cv2_to_imgmsg(rendered_img, 'rgb8')
