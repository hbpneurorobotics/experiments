"""
This file handles targets and regions and is called with an event frame
"""

import numpy as np


# A script to filter the events spatially, will depend on the event representation chosen
# Can select clusters by events density/shape
class Region:
    """
    A class for regions : the output of the clustering step in the algorithm,
    its attributes are the number of events belonging to the region,
    its barycentre which is the mean of the events' location and the polarity
    which is also based on events polarity (see compute_polarity(self))
    """

    def __init__(self, pixels):
        self._size = len(pixels)
        self._pixels = pixels  # pixels (tuples (x,y, polarity)) belonging to the region
        self._barycentre = (0, 0)  # region's position on the EBS screen
        self._polarity = 0  # average polarity of the events belonging to the region, might be a useful information to decode the sequence
        self.compute_barycentre()
        self.compute_polarity()

    def compute_barycentre(self):
        self._barycentre = (round(sum([pixel.x for pixel in self._pixels]) / self._size),
                            round(sum([pixel.y for pixel in self._pixels]) / self._size))

    def compute_polarity(self):
        # the polarity is 1 if the majority of region's pixels have a negative polarity, 
        # -1 if the majority of region's pixels have a positive polarity.
        # 0 if the polarity is null on average
        # TODO deal with the different case to know the emitter's state
        self._polarity = round(sum([2 * pixel.polarity - 1 for pixel in self._pixels]) / self._size)


def pixel_neighbourhood(location, width=3):
    """
    A method returning all the width^2-1 locations (tuples) directly around a pixel
    """
    x, y = location[0], location[1]
    offset = width // 2
    return tuple((i, j)
                 for i in range(max(0, x - offset), min(128, x + offset + 1))
                 for j in range(max(0, y - offset), min(128, y + offset + 1))
                 if (i, j) != (x, y))


def region_labelling_touching_neighbours(event_group):
    """
    Iterative clustering method which cluster each unlabelled event with other events that are in 
    its direct surrounding.
    Then for each new event added to the cluster the step is repeated until no more events are found
    """
    localized_events = np.empty((129, 129), dtype=object)
    locations = []
    # After this localization, we can search in a more efficient ways for neighbours
    for event in event_group:
        localized_events[event.x][event.y] = event
        locations.append(event)

    regions = []
    # print("Number of events : " + str(len(locations)))
    for event in locations:

        if localized_events[event.x][event.y] is not None:
            localized_events[event.x][event.y] = None
            neighbours = [event]
            region_size = 0
            new_neighbour_found = True

            while new_neighbour_found:  # iterative step

                new_neighbours = neighbours[region_size:]
                new_neighbour_found = False
                region_size = len(neighbours)

                # all the neighbours that have been associated in the last iteration are checked
                for nn in new_neighbours:
                    for x, y in pixel_neighbourhood((nn.x, nn.y)):

                        potential_neighbour = localized_events[x][y]
                        if potential_neighbour is not None:  # if there is an event at this location
                            # We don't need to check a time window since we already receive events
                            # in a small time window but this condition could be relevant
                            # for another event representation
                            neighbours.append(potential_neighbour)
                            localized_events[x][y] = None

                new_neighbour_found = region_size < len(neighbours)
            regions.append(neighbours)

    # print("Number of regions : " + str(len(regions)))
    return regions


def region_filtering(regions):
    """
    The filtering method that returns target which are input in the tracking script. Only regions with a polarity
    not null are kept, with a shape that is roughly circular.
    """
    targets = []
    for region in regions:
        count = region._size
        if count == 1:  # if there is only one event in the region, no shape filter is apply
            targets.append(region)
        elif count <= 50:
            if region._polarity != 0:  # filtering the events triggered by motions
                # for each pixel of the region we are computing its distance to the barycentre,
                # and  we are taking the biggest one to draw a circular shape.
                radius = 0
                for pixel in region._pixels:  # we
                    distance = np.linalg.norm((region._barycentre[0] - pixel.x,
                                               region._barycentre[1] - pixel.y))
                    if distance > radius:
                        radius = distance
                shape_area = np.pi * radius ** 2
                if count / shape_area > 0.8:
                    targets.append(region)
            else:
                print("polarity null")

    return targets
